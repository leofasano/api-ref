export class CssController{
    private _lightThemedCss: string = "./css/darkTheme.css"
    private _darkThemedCss: string = "./css/lightTheme.css"

    public cssTheme: string

    constructor(theme: string){
        switch(theme){
            case "dark":
                this.cssTheme = this._darkThemedCss
                break;
            case "light":
                this.cssTheme = this._lightThemedCss
                break;
            default:
                this.cssTheme = this._darkThemedCss
                break;
        }
    }
}