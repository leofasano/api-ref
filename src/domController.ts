import * as process from "process"
import * as path from "path"
import * as fs from "fs"

import AppArgs from "./interfaces/appArgs"
import Dom from "./interfaces/dom";

const showdown  = require('showdown');
const Window = require('window');
const { promisify } = require('util');
const { resolve } = require('path');
const readdir = promisify(fs.readdir);
const stat = promisify(fs.stat);
const pjson = require('../package.json');

export class DomController{
    private _originalSrcDir: string
    private _srcDir: string
    private _outDir: string
    private _outCssDir: string
    private _outJsDir: string
    private _apiHost: string
    private _targetCssFile: string
    private _prettyJsFile: string
    private _mainJsFile: string
    private _possibleTheme:string[] = ["dark", "light", "hybrid"]

    private _args: AppArgs

    private dom: Dom 

    private document = new Window().document
    private converter = new showdown.Converter()

    constructor(args: AppArgs){
        this._args = args
        this._originalSrcDir = args.src
        this._srcDir = path.join(process.cwd(), args.src)
        this._outDir = path.join(process.cwd(), args.outDir)
        this._outCssDir = path.join(this._outDir, "/css")
        this._outJsDir = path.join(this._outDir, "/js")
        this._apiHost = args.apiHost
        this._targetCssFile = args.theme == "light" ? "css/lightTheme.css" : 
            args.theme == "hybrid" ? "css/hybridTheme.css" : "css/darkTheme.css"
        this._prettyJsFile = "js/pretty.js"
        this._mainJsFile = "js/main.js"

        this.dom = {
            sectionContainer: this.document.createElement("section"),
            apiMenu: this.document.createElement("div"),
            menuList: this.document.createElement("ul"),
            contentList: this.document.createElement("div"),
            cssLink: this.document.createElement("link"),
            jsLink: this.document.createElement("script"),
            jsPrettyLink: this.document.createElement("script"),
            fontLink: this.document.createElement("link"),
        }

    }

    private logAsciiArtLogo(){
        console.log('\x1b[34m%s\x1b[0m', "_______________________________________________________________________________________");
        console.log('\x1b[34m%s\x1b[0m', " ");
        console.log('\x1b[34m%s\x1b[0m', "    _____                      _  _    _                    _             _            ");
        console.log('\x1b[34m%s\x1b[0m', "   / ____|                    (_)| |  (_)                  | |           | |           ");
        console.log('\x1b[34m%s\x1b[0m', "  | |      ___    __ _  _ __   _ | |_  _ __   __ ___  ___  | |      __ _ | |__   ___   ");
        console.log('\x1b[34m%s\x1b[0m', "  | |     / _ \\  / _` || '_ \\ | || __|| |\\ \\ / // _ \\/ __| | |     / _` || '_ \\ / __|  ");
        console.log('\x1b[34m%s\x1b[0m', "  | |____| (_) || (_| || | | || || |_ | | \\ V /|  __/\\__ \\ | |____| (_| || |_) |\\__ \\  ");
        console.log('\x1b[34m%s\x1b[0m', "   \\_____|\\___/  \\__, ||_| |_||_| \\__||_|  \\_/  \\___||___/ |______|\\__,_||_.__/ |___/  ");
        console.log('\x1b[34m%s\x1b[0m', "                  __/ |                                                                ");
        console.log('\x1b[34m%s\x1b[0m', "                 |___/                                                                 ");
        console.log('\x1b[34m%s\x1b[0m', "_______________________________________________________________________________________\n\n\n");
    }

    /**
     * Initialize the generated dom with stylesheets, scripts, and dom containers elements 
     */
    private initDom(){
        this.logAsciiArtLogo()
        this.dom.sectionContainer.setAttribute("id", "main-container")
        this.dom.apiMenu.setAttribute("id", "api-menu")
        this.dom.contentList.setAttribute("class", "reference-container")

        this.dom.cssLink.setAttribute("rel", "stylesheet")
        this.dom.cssLink.setAttribute("type", "text/css")
        this.dom.cssLink.setAttribute("href", "css/main.css")

        this.dom.jsPrettyLink.setAttribute("src", "js/pretty.js")
        this.dom.jsLink.setAttribute("src", "js/main.js")

        this.dom.fontLink.setAttribute("rel", "stylesheet")
        this.dom.fontLink.setAttribute("href", "https://fonts.googleapis.com/css2?family=Roboto:wght@100;400&display=swap")
    }

    /**
     * Initialize the ouput directory with all required children directories
     */
    private initOutDir(){
        this.removeDir(this._outDir)
        this.removeDir(this._outCssDir)
        this.removeDir(this._outJsDir)
        if (!fs.existsSync(this._outDir)){
            fs.mkdirSync(this._outDir);
        }
        if (!fs.existsSync(this._outCssDir)){
            fs.mkdirSync(this._outCssDir);            
        }
        if (!fs.existsSync(this._outJsDir)){
            fs.mkdirSync(this._outJsDir);            
        }
    }

    /**
     * Remove a directory
     * @param dirPath The path of the directory to remove
     */
    private removeDir(dirPath: string) {
        if (!fs.existsSync(dirPath)) {
            return;
        }

        var list = fs.readdirSync(dirPath);
        for (var i = 0; i < list.length; i++) {
            var filename = path.join(dirPath, list[i]);
            var stat = fs.statSync(filename);

            if (filename == "." || filename == "..") {
                // do nothing for current and parent dir
            } else if (stat.isDirectory()) {
                this.removeDir(filename);
            } else {
                fs.unlinkSync(filename);
            }
        }

        fs.rmdirSync(dirPath);
    };

    /**
     * Create a stylized json for dom integration
     * @param json the json to stylize
     * @param light the style theme to apply
     */
    private getStylizedJson(json: string, light?: boolean){
        json = json.replace(/<pre>/g, "<pre class='prettyprint'>")
        return json
    }

    /**
     * Create a span with the route corresponding class and uri params
     * @param route the route to stylize
     */
    private getStylizedRoute(route: string) {
        let paramsMatchs = route.match(/(\[(.*?)\])/g)

        if(paramsMatchs != null){
            for(let i = 0; i < paramsMatchs.length; i++){
                route = route.replace(paramsMatchs[i], "<span class='uri-params'>"+paramsMatchs[i].slice(0, paramsMatchs[i].length)+"</span>")
            }
        }
        return route
    }

    /**
     * Return the route type's class to apply
     * @param routeType The route type (GET, POST, PUT or DELETE)
     */
    private getRouteTypeClass(routeType: string){
        if(routeType.indexOf("post") != -1){
            return "route-type-post"
        }
        if(routeType.indexOf("get") != -1){
            return "route-type-get-light"
        }
        if(routeType.indexOf("put") != -1){
            return "route-type-put-light"
        }
        if(routeType.indexOf("delete") != -1){
            return "route-type-delete"
        }
        if(routeType.indexOf("patch") != -1){
            return "route-type-patch"
        }
    }

    private async getFiles(dir: string) {
        const subdirs = await readdir(dir);
        const files = await Promise.all(subdirs.map(async (subdir: any) => {
          const res = resolve(dir, subdir);
          return (await stat(res)).isDirectory() ? this.getFiles(res) : res;
        }));
        return files.reduce((a: any, f: any) => a.concat(f), []);
      }

    /**
     * Generate the api reference files
     */
    private createDom(){
        let _this: DomController = this
            let routeTotalCount: number = 0

            _this.getFiles(_this._srcDir)
                .then((fileList) => {
                    let files: string[] = fileList as string[]
                    let max: number = files.length-1
                    let i: number = 0
                    //listing all files using forEach
                    files.forEach(function (file) {
                        if(file.indexOf(".js") != -1 || file.indexOf(".ts") != -1 || file.indexOf(".txt") != -1){
                            let fileData = fs.readFileSync(file, 'utf8')
                            let originalFileData = fileData
                            console.log('\x1b[36m%s\x1b[0m', "Reading file "+file.slice(file.lastIndexOf("/")+1, file.length));
                            
                            var routeCount = (fileData.match(/@ApiReference/g) || []).length;
                            routeTotalCount += routeCount
            
                            if(originalFileData.indexOf("@ApiReference") != -1){
                                fileData = fileData.slice(fileData.indexOf("@ApiGroupe"), fileData.length)
                                let apiGroup = fileData.slice(fileData.indexOf("@ApiGroupe")+11, fileData.indexOf("\n"))
                                fileData = fileData.slice(fileData.indexOf("@ApiGroupeDescription"), fileData.length)
                                let apiDescGroup = fileData.slice(fileData.indexOf("@ApiGroupeDescription")+22, fileData.indexOf("\n"))
                                let groupElement = _this.document.createElement("li")
                                groupElement.innerHTML = apiGroup
                                for(let i = 0; i < routeCount; i++){
                                    fileData = fileData.slice(fileData.indexOf("@route"), fileData.length)
                                    let routeType = fileData.slice(fileData.indexOf("@route")+7, fileData.indexOf("/"))
            
                                    fileData = fileData.slice(fileData.indexOf("@route"), fileData.length)
                                    let route = fileData.slice(fileData.indexOf("/"), fileData.indexOf("\n"))
            
                                    fileData = fileData.slice(fileData.indexOf("@headers"), fileData.length)
                                    let headers = fileData.slice(fileData.indexOf("@headers")+9, fileData.indexOf("\n"))
            
                                    fileData = fileData.slice(fileData.indexOf("@title"), fileData.length)
                                    let title = fileData.slice(fileData.indexOf("@title")+7, fileData.indexOf("\n"))
            
                                    fileData = fileData.slice(fileData.indexOf("@title"), fileData.length)
                                    let markdownDescription = fileData.slice(fileData.indexOf("@description")+12, fileData.indexOf("@responses"))
            
                                    fileData = fileData.slice(fileData.indexOf("@responses"), fileData.length)
                                    let markdownHttpResponses = fileData.slice(fileData.indexOf("@responses")+10, fileData.indexOf("@exemple"))
            
                                    fileData = fileData.slice(fileData.indexOf("@exemple"), fileData.length)
                                    let jsonMarkdownExemple = fileData.slice(fileData.indexOf("@exemple")+9, fileData.indexOf("*/"))
            
                                    fileData = fileData.slice(fileData.indexOf("@ApiReference"), fileData.length)
            
                                    let listElement = _this.document.createElement("li")
                                    listElement.setAttribute("class", title.replace(/\s/g, "-"))
                                    let linkElement = _this.document.createElement("a")
                                    linkElement.innerHTML = title
                                    linkElement.setAttribute("href", "#"+title)
                                    if(i == 0){
                                        groupElement.setAttribute("class", "list-group")
                                        _this.dom.menuList.appendChild(groupElement)
                                    }
                                    listElement.appendChild(linkElement)
                                    _this.dom.menuList.appendChild(listElement)
            
                                    let reference = _this.document.createElement("div")
                                    reference.setAttribute("class", "row")
                                    reference.setAttribute("id", title)
            
                                    let description = _this.document.createElement("div")
                                    description.setAttribute("class", "description")
                                    let descriptionTitle = _this.document.createElement("h2")
                                    descriptionTitle.innerHTML = "<span class='route-type-"+routeType.toLowerCase()+"'>"+ routeType +"</span>"+title
            
            
                                    let codeExemple = _this.document.createElement("div")
                                    codeExemple.setAttribute("class", "exemple")
            
            
            
                                    let jsonExemple = _this.document.createElement("div")
                                    jsonExemple.setAttribute("class", "jsonExemple")
                                    jsonExemple.innerHTML = "<span class='"+_this.getRouteTypeClass(routeType.toLowerCase())+"'>" + routeType + "</span>" + _this.getStylizedRoute(route) + "<br>HTTP/1.1<br><br><span class='headers'>Host</span>:<br>"+_this._apiHost+"<br>"+(headers.length > 1 ? "<br><span class='headers'>Headers</span>:<br>"+headers : "")+(jsonMarkdownExemple.length > 1 ? "<br><br><span class='headers body'>Body</span>:"+_this.getStylizedJson(_this.converter.makeHtml(jsonMarkdownExemple)) : "");
                                    let exempleLowTitle = _this.document.createElement("div")
                                    exempleLowTitle.setAttribute("class", "exemple-low-title")
                                    let exempleLowTitleLeft = _this.document.createElement("span")
                                    let exempleLowTitleRight = _this.document.createElement("span")
                                    exempleLowTitleRight.setAttribute("class", "light")
            
                                    exempleLowTitleLeft.innerHTML = "Request exemple"
                                    exempleLowTitleRight.innerHTML = title
            
                                    exempleLowTitle.appendChild(exempleLowTitleLeft)
                                    exempleLowTitle.appendChild(exempleLowTitleRight)
            
                                    codeExemple.appendChild(exempleLowTitle)
                                    codeExemple.appendChild(jsonExemple)
            
                                    description.innerHTML = _this.converter.makeHtml(markdownDescription);
                                    let response = _this.document.createElement("div")
                                    response.innerHTML = _this.converter.makeHtml(markdownHttpResponses);
                                    description.prepend(descriptionTitle)
                                    if(i == 0){
                                        let titleReference = _this.document.createElement("div")
                                        titleReference.setAttribute("class", "row row-title")
            
                                        let descriptionGroup = _this.document.createElement("div")
                                        descriptionGroup.setAttribute("class", "description")
            
                                        let groupTitle = _this.document.createElement("h2")
                                        groupTitle.setAttribute("class", "group-title")
                                        groupTitle.innerHTML = apiGroup
            
                                        let groupDesc = _this.document.createElement("div")
                                        groupDesc.setAttribute("class", "group-desc")
                                        groupDesc.innerHTML = _this.converter.makeHtml(apiDescGroup)
                                        descriptionGroup.prepend(groupDesc)
                                        descriptionGroup.prepend(groupTitle)
            
                                        let codeExemple = _this.document.createElement("div")
                                        codeExemple.setAttribute("class", "exemple")
            
                                        titleReference.appendChild(descriptionGroup)
                                        titleReference.appendChild(codeExemple)
            
                                        _this.dom.contentList.appendChild(titleReference)
                                    }
                                    let exemple = _this.document.createElement("div")
                                    exemple.setAttribute("class", "body-exemple")
                                    let exempleTitle = _this.document.createElement("h3")
                                    exempleTitle.innerHTML = "Body exemple(s)"
                                    exemple.innerHTML = _this.getStylizedJson(_this.converter.makeHtml(jsonMarkdownExemple), true)
                                    exemple.prepend(exempleTitle)
                                    if(jsonMarkdownExemple.length > 1){
                                        description.appendChild(exemple)
                                    }
            
                                    description.appendChild(response)
            
                                    reference.appendChild(description)
                                    reference.appendChild(codeExemple)
            
                                    _this.dom.contentList.appendChild(reference)
                                }
            
                                console.log('\x1b[32m%s\x1b[0m', '  ==> Generated reference from '+file.slice(file.lastIndexOf("/")+1, file.length));
                            }                
            
                            fileData = originalFileData
            
                            if(originalFileData.indexOf("@ApiSummary") != -1){
                                fileData = fileData.slice(fileData.indexOf("@ApiSummary"), fileData.length)
                                let summary = fileData.slice(fileData.indexOf("@ApiSummary")+11, fileData.indexOf("*/"))
                                summary = _this.converter.makeHtml(summary)
            
                                let summaryRow = _this.document.createElement("div")
                                summaryRow.setAttribute("class", "row")
            
                                
                                let description = _this.document.createElement("div")
                                description.setAttribute("class", "description")
            
            
                                let codeExemple = _this.document.createElement("div")
                                codeExemple.setAttribute("class", "exemple head-exemple")
                                codeExemple.innerHTML = "<p class='api-ref-version'>Api reference generated with @cognitives_things/api-ref v"+pjson.version+"</p>"
            
                                description.innerHTML = summary
                                description.setAttribute("class", "description head")
            
                                summaryRow.appendChild(description)
                                summaryRow.appendChild(codeExemple)
            
                                _this.dom.contentList.prepend(summaryRow)
            
                                console.log('\x1b[32m%s\x1b[0m', '  ==> Generated API header from '+file.slice(file.lastIndexOf("/")+1, file.length));
                            }
            
                            if(i >= max){
                                _this.dom.apiMenu.appendChild(_this.dom.menuList)
                                _this.dom.sectionContainer.appendChild(_this.dom.apiMenu)
                                _this.dom.sectionContainer.appendChild(_this.dom.contentList)
                                _this.document.head.appendChild(_this.dom.cssLink)
                                _this.document.head.appendChild(_this.dom.jsPrettyLink)
                                _this.document.head.appendChild(_this.dom.jsLink)
                                _this.document.title = "Api reference"
                                _this.document.head.appendChild(_this.dom.fontLink)
                                _this.document.body.appendChild(_this.dom.sectionContainer)
            
                                fs.writeFile(_this._outDir+"/js/pretty.js", fs.readFileSync(path.join(__dirname, _this._prettyJsFile)), function (err) {
                                    if(err != null){
                                        console.log('\x1b[32m%s\x1b[0m', "Error while generation of the pretty js file", err)
                                    }
                                })
                                fs.writeFile(_this._outDir+"/js/main.js", fs.readFileSync(path.join(__dirname, _this._mainJsFile)), function (err) {
                                    if(err != null){
                                        console.log('\x1b[32m%s\x1b[0m', "Error while generation of the main js file", err)
                                    }
                                })
            
                                fs.writeFile(_this._outDir+"/css/main.css", fs.readFileSync(path.join(__dirname, _this._targetCssFile)), function (err) {
                                    if (err) {
                                        console.log('\x1b[32m%s\x1b[0m', "Error while generation of the main css file", err)
                                    }
                                    else{
                                        fs.writeFile(_this._outDir+"/index.html", "<html>"+_this.document.documentElement.innerHTML+"</html>", function (err) {
                                            if(err != null){
                                                console.log('\x1b[32m%s\x1b[0m', file+' cannot be generated', err)
                                            }
                                            else{
                                                let bar = "================================================"
                                                for(let i = 0; i <= _this._originalSrcDir.length; i++){
                                                    bar += "="
                                                }
                                                console.log('\x1b[32m%s\x1b[0m', "\n\n"+bar);
                                                console.log('\x1b[32m%s\x1b[0m', '   API Reference generated sccessfully from ./'+path.join(_this._args.src, "*"));
                                                console.log('\x1b[32m%s\x1b[0m', bar);
                                                console.log('\x1b[36m%s\x1b[0m', '   Output directory: '+_this._args.outDir+'   ');
                                                console.log('\x1b[36m%s\x1b[0m', '   '+routeTotalCount+' routes referenced   ');
                                                let themeApplied = _this._possibleTheme.indexOf(
                                                    _this._args.theme
                                                ) != -1 ? _this._args.theme : _this._possibleTheme[0]
                                                console.log('\x1b[36m%s\x1b[0m', '   '+themeApplied+' theme applied   ');
                                                console.log('\x1b[32m%s\x1b[0m', bar);
                                            }
                                        });  
                                    }
                                });            
                            }
                            i++
                        }
                    });
                })
                .catch((err) => {
                    console.log("\x1b[31m%s\x1b[0m", "Unable to read directory "+_this._srcDir+":", err)
                })
    }

    /**
     * Input function to generate the api reference
     */
    public generateApiReferenceDom(){
        this.initOutDir()
        this.initDom()
        this.createDom()
    }
}