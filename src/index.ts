import { ArgController } from "./argController"
import { CssController } from "./cssController"
import { DomController } from "./domController"

import AppArgs from "./interfaces/appArgs"

const argController = new ArgController()
const cssController = new CssController(
    argController.flags.theme
)
const domController = new DomController(
    argController.flags as AppArgs
)

domController.generateApiReferenceDom()

