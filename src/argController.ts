import args from 'args'
import * as process from 'process'


export class ArgController{
    public flags: any
    public pjson: any

    constructor(){
        args
        .option("theme", 'define the output website css theme', "dark")
        .option("outDir", 'define the api reference output directory', "./apiReference")
        .option("src", 'define the file in which to search for an api reference to generate', "./src")
        .option("apiHost", 'define the api endpoint host', "https://api-host.com")

        this.flags = args.parse(process.argv)
        this.pjson = require('../package.json');
    }
}