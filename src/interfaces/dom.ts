export default interface Dom{ 
    sectionContainer: HTMLElement,
    apiMenu: HTMLDivElement,
    menuList: HTMLUListElement,
    contentList: HTMLDivElement,
    cssLink: HTMLLinkElement,
    jsPrettyLink: HTMLScriptElement,
    jsLink: HTMLScriptElement,
    fontLink: HTMLLinkElement 
}