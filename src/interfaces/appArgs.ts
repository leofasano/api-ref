export default interface AppArgs{
    theme: string
    outDir: string
    src: string
    apiHost: string
}