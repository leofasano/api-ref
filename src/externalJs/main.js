window.onscroll = function (e) { 
    var selectedElement
    for(let el of window.document.getElementsByClassName("row")){
        if(elementInViewport(el) && el.id != null && el.id.length > 0){
            selectedElement = el
        }
    }

    for(let lisEl of window.document.getElementById("api-menu").children[0].children){
        let targetId = selectedElement != null ? selectedElement.id : null
        if(selectedElement != null && lisEl.className.indexOf(targetId.replace(/\s/g, "-")) != -1){
            lisEl.classList.add("selected");
        }
        else if(selectedElement != null){
            lisEl.classList.remove("selected");
        }
    }
} 

function elementInViewport(el) {
    var top = el.offsetTop;
    var left = el.offsetLeft;
    var width = el.offsetWidth;
    var height = el.offsetHeight;
  
    while(el.offsetParent) {
      el = el.offsetParent;
      top += el.offsetTop;
      left += el.offsetLeft;
    }
  
    return (
      top >= window.pageYOffset &&
      left >= window.pageXOffset &&
      (top + height) <= (window.pageYOffset + window.innerHeight) &&
      (left + width) <= (window.pageXOffset + window.innerWidth)
    );
}