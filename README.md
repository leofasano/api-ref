# Cognitives Labs API reference generator

@cognitives_things/api-ref is a TypeScript package for the generation of API references based on JavaScript / TypeScript comments.

## Installation

To start working with the API reference generator, you will need to install the package globally:  

```bash
npm i -g @cognitives_things/api-ref
```

You can create an alias to make this package easier to use as follows:
```bash
alias apiref='sudo node /usr/local/lib/node_modules/@cognitives_things/api-ref/dist/index.js'
```

## Use the generator

To generate a API reference, you have to use the command as a super user to allow the generator to delete and create files and directories

```bash
sudo apiref
```

### Flags

Src flag allows you to specify the folder in which to read apidoc in order to generate the API reference.
```bash
--src, -s
```  
Output directory flag allow you to specify an output directory to store the api reference website files.
```bash
--outDir, -o
```  
Api host flag allow you to specify the concerned api endpoint. 
```bash
--apiHost, -a
```
Theme flag allow you to specify the theme you want for the output api reference website. 
Allowed values are: 
* dark
* light
* hybrid
```bash
--theme, -t
```  


## Code comments

> @ApiSummary support markdown comment.  
> @description support markdown comment.  
> @responses support markdown comment.  
> @exemple support markdown comment.  

### API summary

> You can specify an api summary to generate a description header to your api reference.

#### Exemple
    /**
    @ApiSummary
    # My API reference

    > my api is a cool project.  
    > This API reference contain all routes definitions to use the API.

    > To learn more about the code, go to <a href="#">this github repo</a>.
    */

### API Groupe

> You can specify route groups to create header and order in the api tree structure.

#### Exemple
    /**
    @ApiGroupe User routes
    @ApiGroupeDescription > "User" routes allow you to interact with user's data.
    */

### API route

> You have to specify all the reference for each API's routes.

#### Comment markers
    @ApiReference identify the begin of a route API reference generation.
    @route identify the URI route.
    @headers identify all the headers required for the route.
    @title identify the title/name of the route.
    @description identify the route description, it support markdown.
    @responses identify the possible API response, it support markdown.
    @exemple identify body exemple(s), it support markdown.

> All the comments markers are required but can be empty.

#### Exemple

    /**
    @ApiReference
    @route POST /api/user
    @headers Authorization: YOUR_TOKEN
    @title Create a new gate
    @description
    ### Description

    > This route allows to an <a>ADMIN</a> user to create a new gate.

    ### Body strucure:

    > userUuid: <a href="your-model-url">String</a>  
    > userFirstName: <a href="your-model-url">String</a>  
    > userLastName: <a href="your-model-url">String</a>  
    > userBirth: <a href="your-model-url">Date</a>  
    > userWeight: <a href="your-model-url">Float</a>  

    ###### Note
    > It is not recommended to use more than two decimal for "userWeight" propertie. </a>. 

    @responses
    ### HTTP Response:
    > <a>String</a>  
    > "User created"
    
    @exemple
    ```json
    {
        userUuid: "user uuid", 
        userFirstName: "jhon",
        userLastName: "doe",
        userBirth: "01/01/1950",
        userWeight: 82.5
    }
    ```
    */
